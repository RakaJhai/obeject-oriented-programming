/*
SOAL: BUAT DALAM BENTUK CLASS
1. Ada sebuah mobil berwarna merah yang bisa melaju dengang kecepatan 250km/jam
*/
class Mobil{
    constructor(warna, laju){
        this.warna = warna;
        this.laju = laju;
    }

    warnaMobil = () => {
        console.log(`Ini mobil saya, berwarna ${this.warna}`)
    }

    lajuMobil(){
        this.warnaMobil();
        console.log(`Mobil saya berkecepatan ${this.laju}`)
    }
}

const printMobil = new Mobil("Hitam", "250");
printMobil.lajuMobil();