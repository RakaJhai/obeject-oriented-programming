/*
SOAL: BUAT DALAM BENTUK CLASS
2. Ada sebuah motor biru dan beroda dua, yang bisa melaju 125km/jam
*/

class Motor{
    constructor(warna, laju){
        this.warna = warna;
        this.laju = laju;
    }

    cetakWarna(){
        console.log(`Motor saya berwarna ${this.warna}`)
    }

    cetakLaju(){
        console.log(`Motor saya berkecepatan ${this.laju} km/jam`)
    }
}

class Roda extends Motor{
    constructor(warna, laju, roda){
        super(warna, laju);
        this.roda = roda;
    }

    cetakRoda(){
        console.log(`Motor saya mempunyai Roda ${this.roda}`)
    }
}

// const warna = new Motor('Biru','125');
// warna.cetakWarna();

const roda = new Roda('Biru', '125', 2);
roda.cetakWarna();
roda.cetakLaju();
roda.cetakRoda();